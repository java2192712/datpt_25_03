package com.example.datpt_25_03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Datpt2503Application {

    public static void main(String[] args) {
        SpringApplication.run(Datpt2503Application.class, args);
    }

}
