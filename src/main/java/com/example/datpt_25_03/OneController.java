package com.example.datpt_25_03;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/b1")
public class OneController {
    @GetMapping("/namNhuan")
    public String checkLeapYear(@RequestParam int nam) {
        if ((nam % 4 == 0 && nam % 100 != 0) || (nam % 400 == 0)) {
            return nam + " là năm nhuận.";
        } else {
            return nam + " không phải là năm nhuận.";
        }
    }
}
